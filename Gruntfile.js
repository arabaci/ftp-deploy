module.exports = function(grunt) {
  require("load-grunt-tasks")(grunt);
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    shell: {
      npm_test_jest: {
        command: "npm run build"
      }
    },
    "ftp-deploy": {
      build: {
        auth: {
          host: "40.89.165.125",
          port: 21,
          authKey: "key1"
        },
        src: "dist",
        dest: "web/panel.mp.ofinansdev.com/public_html/"
      }
    }
  });
  grunt.loadNpmTasks("grunt-ftp-deploy");

  grunt.registerTask("default", ["ftp-deploy:build"]);
};
