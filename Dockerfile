FROM node:16.16.0-alpine

WORKDIR /frontend

COPY package*.json ./

RUN yarn install

COPY . ./

RUN yarn build:dev

CMD yarn serve
