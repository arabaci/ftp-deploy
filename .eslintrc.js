module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/essential', '@vue/prettier'],
  rules: {
    "singleQuote": true,
    // "quotes": ["error", "single"],
    "no-console": 0,
    quotes: 'off',
    // 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // 'no-console': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // "no-unused-vars":
    'max-len': ['error', { code: 360 }],
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};
