import Vue from "vue";
import Router from "vue-router";
import store from "@/store";
import { USER_ROLE } from "./../constants/roles";

Vue.use(Router);

const isAuthenticated = (to, from, next) => {
  if (store.getters["isAuthenticated"]) {
    next();
    return;
  }
  next("/login");
};

const isNotAuthenticated = (to, from, next) => {
  if (!store.getters["isAuthenticated"]) {
    next();
    return;
  }
  next("/dashboard");
};

const checkRole = role => {
  const userRole = store.getters["userRole"];
  const hasAccess = userRole[role];
  return hasAccess ? undefined : "/";
};

const checkIsDist = (to, from, next) => {
  if (store.getters["isDistributor"]) {
    next();
    return;
  }
  next("/dashboard");
};

const checkIsDistOrMerchant = (to, from, next) => {
  if (store.getters["isDistributor"] || store.getters["isMerchant"]) {
    next();
    return;
  }
  next("/dashboard");
};

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      redirect: "/dashboard",
      component: () => import("@/view/layout/MainLayout"),
      beforeEnter: isAuthenticated,
      children: [
        {
          path: "/dashboard",
          name: "dashboard",
          component: () => import("@/view/pages/Dashboard.vue")
        },
        {
          path: "/sales-management",
          component: () => import("@/view/layout/CommonLayout"),
          beforeEnter: (to, from, next) => {
            next(checkRole(USER_ROLE.CAN_SELL_QR_CODE));
          },
          children: [
            {
              path: "list",
              name: "sales-list",
              component: () => import("@/view/pages/sales-management/List.vue")
            },
            {
              path: "link-sales",
              name: "link-sales",
              component: () =>
                import("@/view/pages/sales-management/Link-sales.vue")
            },
            {
              path: "order-details",
              name: "order-details",
              component: () =>
                import("@/view/pages/sales-management/OrderDetails.vue")
            },
            {
              path: "qr-sale",
              name: "qr-sale",
              component: () =>
                import("@/view/pages/sales-management/Qr-sale.vue")
            }
          ]
        },
        {
          path: "/personel-management/list",
          name: "personel-management",
          component: () => import("@/view/pages/personel-management/List.vue")
        },
        {
          path: "/personel-management/add",
          name: "personel-add",
          component: () => import("@/view/pages/personel-management/Add.vue")
        },
        {
          path: "/merchant-branch-report-list",
          name: "merchant-branch-report-list",
          component: () =>
            import("@/view/pages/sales-management/Branch-Report-List.vue"),
          beforeEnter: checkIsDistOrMerchant
        },
        {
          path: "/wallet-management/list",
          name: "wallet-list",
          component: () => import("@/view/pages/wallet-management/List.vue"),
          beforeEnter: (to, from, next) => {
            next(checkRole(USER_ROLE.HAS_WALLETS));
          }
        },
        {
          path: "/wallet-management/withdrawal",
          name: "wallet-withdrawal",
          component: () =>
            import("@/view/pages/wallet-management/Withdrawal.vue"),
          beforeEnter: (to, from, next) => {
            next(checkRole(USER_ROLE.HAS_WALLETS));
          }
        },
        {
          path: "/corporate",
          component: () => import("@/view/layout/CommonLayout"),
          beforeEnter: (to, from, next) => {
            next(checkRole(USER_ROLE.CAN_SUB_DEAL));
          },
          children: [
            {
              path: "companies",
              name: "company-companies",
              component: () => import("@/view/pages/company/Companies.vue")
            },
            {
              path: "companies/deleted",
              name: "company-companies-deleted",
              component: () => import("@/view/pages/company/Deleted.vue")
            },
            {
              path: "companies/company",
              name: "company-companies-company",
              component: () => import("@/view/pages/company/Company.vue")
            },
            {
              path: "companies/company/users/:companyId",
              name: "company-companies-company-users",
              component: () => import("@/view/pages/company/Users.vue"),
              props: true
            },
            {
              path: "companies/company/users/deleted",
              name: "company-companies-company-users-deleted",
              component: () => import("@/view/pages/settings/Deleted.vue"),
              props: true
            },
            {
              path: "sales-representative-list",
              name: "sales-representative-list",
              component: () =>
                import("@/view/pages/sales-representative/List.vue"),
              beforeEnter: checkIsDist
            },
            {
              path: "sales-representative",
              name: "sales-representative",
              component: () =>
                import("@/view/pages/sales-representative/Representative.vue"),
              beforeEnter: checkIsDist,
              props: true
            }
          ]
        },
        {
          path: "/pos",
          component: () => import("@/view/layout/CommonLayout"),
          children: [
            {
              path: "pos-list",
              name: "pos-list",
              component: () => import("@/view/pages/pos/PosList.vue")
            },
            {
              path: "pos-details",
              name: "pos-details",
              component: () => import("@/view/pages/pos/PosDetails.vue")
            }
          ]
        },
        // {
        //   path: "/pos",
        //   component: () => import("@/view/layout/CommonLayout"),
        //   beforeEnter: checkIsDist,
        //   children: [
        //     {
        //       path: "pos-list",
        //       name: "pos-list",
        //       component: () => import("@/view/pages/pos/PosList.vue")
        //     }
        //   ]
        // },
        {
          path: "/settings/general",
          name: "settings-general",
          component: () => import("@/view/pages/settings/Company.vue"),
          props: true
        },
        {
          path: "/settings/users",
          name: "settings-users",
          component: () => import("@/view/pages/settings/Users.vue"),
          props: true
        },
        {
          path: "/settings/users/user",
          name: "settings-users-user",
          component: () => import("@/view/pages/settings/User.vue"),
          props: true
        },
        {
          path: "/settings/profile",
          name: "settings-users-user-profile",
          component: () => import("@/view/pages/settings/User.vue"),
          props: true
        },
        {
          path: "/settings/users/deleted",
          name: "settings-users-deleted",
          component: () => import("@/view/pages/settings/Deleted.vue"),
          props: true
        },
        {
          path: "/settings/logs",
          name: "settings-logs",
          component: () => import("@/view/pages/settings/Logs.vue")
        },
        {
          path: "/change-password",
          name: "change-password",
          component: () => import("@/view/pages/ChangePassword.vue")
        }
      ]
    },
    {
      path: "/",
      component: () => import("@/view/pages/Login"),
      beforeEnter: isNotAuthenticated,
      children: [
        {
          name: "login",
          path: "/login",
          component: () => import("@/view/pages/Login")
        },
        {
          name: "register",
          path: "/register",
          component: () => import("@/view/pages/Login")
        }
      ]
    },
    {
      name: "api-doc",
      path: "/api-doc",
      component: () => import("@/view/pages/ApiDoc")
    },
    {
      path: "/forgot-password",
      name: "forgot-password",
      component: () => import("@/view/pages/ForgotPassword.vue"),
      beforeEnter: isNotAuthenticated
    },
    {
      path: "*",
      redirect: "/404"
    },
    {
      // the 404 route, when none of the above matches
      path: "/404",
      name: "404",
      component: () => import("@/view/pages/Error.vue")
    }
  ]
});
