import Api from "@/services/api";
import { ExtractRequestData } from "@/utils/extract-request";

export const ListMixin = {
  data() {
    return {
      isBusy: false,
      perPage: 0,
      listData: [],
      currentPage: 1,
      totalRows: 0,
      ordersCount: 0,
      ordersTotal: 0,
      filterIncludedFields: "",
      items: [],
      tableItems: [],
      filter: null,
      filters: [],
      showSearch: false,
      fields: [],
      startDate: null,
      endDate: null,
      include: [],
      transitionProps: {
        name: "flip-list"
      }
    };
  },
  computed: {
    optionFilters() {
      return this.filters.filter(filter => filter.currentValue !== undefined);
    }
  },
  methods: {
    async fetchData(context) {
      const { currentPage, filter, sortBy, sortDesc } = context;
      const includeLength = this.include.length;
      let optionParams = "";
      let filterParams = "";
      let sortParams = "";
      let dateParams = "";
      let includeParams = "";
      let sortDirection = sortDesc ? "desc" : "asc";

      this.optionFilters.forEach(optionFilter => {
        optionParams += `&${optionFilter.field}=${optionFilter.currentValue}`;
      });

      if (this.startDate) {
        dateParams += `&createdAt[after]=${this.startDate}`;
      }

      if (this.endDate) {
        dateParams += `&createdAt[before]=${this.endDate}`;
      }

      if (filter) {
        /* this.filterIncludedFields.forEach(filterIncludedField => {
          filterParams += `&${filterIncludedField}=${this.filter}`;
        }); */

        filterParams += `&${this.filterIncludedFields}=${this.filter}`;
      }

      const sortById = sortBy === "_id" ? "id" : sortBy;

      if (sortById) {
        sortParams += `&order[${sortById}]=${sortDirection}`;
      }

      if (includeLength) {
        includeParams = "&include=";
        this.include.forEach((includeItem, index) => {
          includeParams += `${includeItem}`;

          if (index + 1 < includeLength) {
            includeParams += ",";
          }
        });
      }

      const fixedFilters = this.fixedFilters ? `&${this.fixedFilters}` : "";

      const pageParam = `?page=${currentPage}`;
      const queryParams =
        pageParam +
        includeParams +
        filterParams +
        fixedFilters +
        optionParams +
        sortParams +
        dateParams;

      const fetchedData = await Api.get(
        this.fetchUrl,
        queryParams,
        Api.JsonApiHeaders
      );

      const { data, totalItems, itemsPerPage } = ExtractRequestData(
        fetchedData
      );

      this.listData = data;
      this.totalRows = totalItems;
      this.perPage = itemsPerPage;
      window.scrollTo(0, 0);
      return data;
    },
    onOptionSelected(filterIndex, value) {
      this.filters[filterIndex].currentValue = value;
      this.$refs.listTable.refresh();
    },
    onFilterChange(filter) {
      this.filter = filter;
    },
    onAllDatesSelected(checked) {
      const hasStartDate = this.startDate !== null;
      const hasEndDate = this.endDate !== null;

      const hasAnyDate = hasStartDate || hasEndDate;

      if (checked === "true" && hasAnyDate) {
        this.startDate = null;
        this.endDate = null;
        this.$refs.listTable.refresh();
      }
    },
    onStartDateSelected(startDate) {
      this.startDate = startDate;
      this.$refs.listTable.refresh();
    },
    onEndDateSelected(endDate) {
      this.endDate = endDate;
      this.$refs.listTable.refresh();
    },
    onRowSelected(/* item */) {},
    onRefreshed() {}
  }
};
