export const GET_DASHBOARD_SUMMARY_REPORT = {
  firstDayOfMonth: "2022-12-01 00:00:00",
  nowDate: "2022-12-15 23:59:59",
  summaryReport: {
    success: true,
    message: "Successfully",
    data: [
      {
        id: 11,
        paymentUnit: "USD",
        totalPrice: "172042.91000000",
        payedPrice: "593.70080000",
        waitingPrice: "171436.72560000"
      },
      {
        id: 111,
        paymentUnit: "GBP",
        totalPrice: "20.80000000",
        payedPrice: "608.05000000",
        waitingPrice: "20.80000000"
      },
      {
        id: 13,
        paymentUnit: "TRY",
        totalPrice: "233.80000000",
        payedPrice: "72.60000000",
        waitingPrice: "161.20000000"
      }
    ]
  },
  branchSummaryRepor: {
    success: true,
    message: "Successfully",
    data: [
      [
        {
          branchId: 137,
          branchTitle: "Test Branch ",
          paymentUnit: "USD",
          totalPrice: "848.48000000",
          payedPrice: "608.05000000",
          waitingPrice: "569.91000000"
        },
        {
          branchId: 137,
          branchTitle: "Test Branch ",
          paymentUnit: "GBP",
          totalPrice: "20.80000000",
          payedPrice: "608.05000000",
          waitingPrice: "569.91000000"
        },
        {
          branchId: 137,
          branchTitle: "Test Branch ",
          paymentUnit: "TRY",
          totalPrice: "233.80000000",
          payedPrice: "608.05000000",
          waitingPrice: "569.91000000"
        }
      ],
      [
        {
          branchId: 138,
          branchTitle: "other branch",
          paymentUnit: "USD",
          totalPrice: "171194.43000000",
          payedPrice: "58.25080000",
          waitingPrice: "171146.57560000"
        }
      ]
    ]
  }
};
