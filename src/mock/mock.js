import Vue from "vue";

var MockAdapter = require("axios-mock-adapter");

// mock testing user accounts
const users = [
  {
    email: "admin@demo.com",
    password: "demo",
    phone: "+905420000000",
    token: "mgfi5juf74j"
  },
  {
    email: "admin2@demo.com",
    password: "demo",
    token: "fgj8fjdfk43"
  }
];

const products = [
  {
    id: "8",
    product_title: "Just another product name",
    product_price: "$178.00",
    product_image:
      "https://www.testjsonapi.com/wp-content/themes/testjsonapi/assets/images/items/8.jpg",
    product_description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    created_at: "2021-02-01 12:42:08",
    updated_at: "2021-02-01 12:42:08"
  },
  {
    id: "7",
    product_title: "Great product name here",
    product_price: "$56.00",
    product_image:
      "https://www.testjsonapi.com/wp-content/themes/testjsonapi/assets/images/items/7.jpg",
    product_description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    created_at: "2021-02-01 12:42:08",
    updated_at: "2021-02-01 12:42:08"
  },
  {
    id: "6",
    product_title: "Some item name here",
    product_price: "$280.00",
    product_image:
      "https://www.testjsonapi.com/wp-content/themes/testjsonapi/assets/images/items/6.jpg",
    product_description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    created_at: "2021-02-01 12:41:08",
    updated_at: "2021-02-01 12:41:08"
  },
  {
    id: "5",
    product_title: "Just another product name",
    product_price: "$179.00",
    product_image:
      "https://www.testjsonapi.com/wp-content/themes/testjsonapi/assets/images/items/5.jpg",
    product_description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    created_at: "2021-02-01 12:41:08",
    updated_at: "2021-02-01 12:41:08"
  },
  {
    id: "4",
    product_title: "Just another product name",
    product_price: "$179.00",
    product_image:
      "https://www.testjsonapi.com/wp-content/themes/testjsonapi/assets/images/items/4.jpg",
    product_description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    created_at: "2021-02-01 12:39:54",
    updated_at: "2021-02-01 12:39:54"
  },
  {
    id: "3",
    product_title: "Great product name here",
    product_price: "$56.00",
    product_image:
      "https://www.testjsonapi.com/wp-content/themes/testjsonapi/assets/images/items/3.jpg",
    product_description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    created_at: "2021-02-01 12:39:54",
    updated_at: "2021-02-01 12:39:54"
  },
  {
    id: "2",
    product_title: "Some item name here",
    product_price: "$280.00",
    product_image:
      "https://www.testjsonapi.com/wp-content/themes/testjsonapi/assets/images/items/2.jpg",
    product_description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    created_at: "2021-02-01 12:38:47",
    updated_at: "2021-02-01 12:38:47"
  },
  {
    id: "1",
    product_title: "Just another product name",
    product_price: "$179.00",
    product_image:
      "https://www.testjsonapi.com/wp-content/themes/testjsonapi/assets/images/items/1.jpg",
    product_description:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    created_at: "2021-02-01 12:38:47",
    updated_at: "2021-02-01 12:38:47"
  }
];

const Mock = {
  init() {
    // this sets the mock adapter on the default instance
    var mock = new MockAdapter(Vue.axios);

    // mock login request
    mock.onPost("/login").reply(data => {
      const credential = JSON.parse(data.data);
      const found = users.find(user => {
        return (
          credential.phone === user.phone &&
          credential.password === user.password
        );
      });
      if (found) {
        return [200, found];
      }
      return [404, { errors: ["The login detail is incorrect"] }];
    });

    // mock to verify authentication
    mock.onGet(/\/verify\/?/).reply(data => {
      const token = data.headers.Authorization.replace("Token ", "");
      if (token !== "undefined") {
        const found = users.find(user => {
          return token === user.token;
        });
        return [200, found];
      }
      return [401, { errors: ["Invalid authentication"] }];
    });

    // mock to products
    mock.onGet(/\/products\/?/).reply(() => {
      return [200, products];
    });
  }
};

export default Mock;
