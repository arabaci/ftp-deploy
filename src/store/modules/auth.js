import Api from "@/services/api";
import ApiOfinans from "@/services/apiOfinans";
import JwtService from "@/services/jwt";
// import Moment from "moment";

export const VERIFY_AUTH = "verifyAuth";
export const OFINANS_LOGIN = "ofinansLogin";
export const LOGIN = "login";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const UPDATE_PASSWORD = "updateUser";

export const PURGE_AUTH = "logOut";
export const SET_AUTH = "setUser";
export const SET_PASSWORD = "setPassword";
export const SET_ERROR = "setError";

export const OFINANS_OTP = "ofinansOtpSend";
export const OFINANS_OTP_AGAIN = "ofinansOtpAgain";
export const OTP = "otpSend";
export const OTP_AGAIN = "otpAgain";
export const APP_LOGIN = "appLogin";

const SAVE_TOKENS = "saveTokens";
// const REFRESH_TOKEN = "refreshToken";
// const START_REFRESH_TIMER = "startRefreshTimer";

// const TOKEN_TTL_HOURS = 12;
// const TOKEN_TTL_MINUTES = TOKEN_TTL_HOURS * 60;
// const TOKEN_TTL_SECONDS = TOKEN_TTL_MINUTES * 60;

// const TOKEN_REFRESH_CHECK_MINUTES = 4;
// const TOKEN_REFRESH_CHECK_SECONDS = TOKEN_REFRESH_CHECK_MINUTES * 60;

// const extractRefreshExpiry = refreshToken => {
//   if (refreshToken) {
//     const hypenIndex = refreshToken.lastIndexOf("_");
//     return refreshToken.slice(hypenIndex + 1);
//   }
// };

// const NOW = () => {
//   return Moment(Date.now()).unix();
// };

// const TOKEN_TTL = () => {
//   const issuedAt = JwtService.getTokenIssuedAt();
//   return parseInt(issuedAt) + TOKEN_TTL_SECONDS;
// };

// const TOKEN_CHECK_AT = () => {
//   return TOKEN_TTL() - TOKEN_REFRESH_CHECK_SECONDS;
// };

// const getExpiryTTL = () => {
//   return TOKEN_CHECK_AT() - NOW();
// };

// const shouldRefresh = () => {
//   return TOKEN_CHECK_AT() <= NOW();
// };

const deStructureParentData = parent => {
  const {
    id,
    title,
    email,
    taxCity,
    taxAdministration,
    taxNumber,
    hasQuickSale,
    role,
    isActive,
    phone,
    nexusMerchantInformation,
    companyRank,
    ordersTotals,
    responsibleAdminId,
    commission
  } = parent;
  return {
    id,
    title,
    email,
    taxCity,
    taxAdministration,
    taxNumber,
    hasQuickSale,
    role,
    isActive,
    phone,
    nexusMerchantInformation,
    companyRank,
    ordersTotals,
    responsibleAdminId,
    commission
  };
};

const deStructureParentRepresentativeData = representative => {
  if (representative) {
    const { id, name, email, phone, isActive } = representative;
    return {
      id,
      name,
      email,
      phone,
      isActive
    };
  }
  return null;
};

const auth = {
  state: () => ({
    errors: null,
    user: {},
    userRole: JSON.parse(sessionStorage.getItem("_userRole") || "{}"),
    company: {},
    isAuthenticated: true,
    refreshTimer: null
  }),
  mutations: {
    [SET_ERROR](state, error) {
      state.errors = error;
    },
    [SET_AUTH](state, user) {
      state.isAuthenticated = true;
      state.user = user;
      state.userRole = user.userRole;
      state.errors = {};
    },
    [SET_PASSWORD](state, password) {
      state.user.password = password;
    },
    [PURGE_AUTH](state) {
      state.isAuthenticated = false;
      state.user = {};
      state.userRole = {};
      state.errors = {};
      JwtService.clearAuth();
    }
  },
  actions: {
    [SAVE_TOKENS](context, data) {
      JwtService.saveAuthToken(data.token);
      // JwtService.saveRefreshToken(data.refresh_token);
      // JwtService.saveTokenIssuedAt(Moment(Date.now()).unix());

      // JwtService.saveRefreshTokenExpiry(
      //   extractRefreshExpiry(data.refresh_token)
      // );
      Api.setHeader();
    },
    // async [REFRESH_TOKEN](context) {
    //   clearInterval(context.state.refreshTimer);
    //   const refreshToken = JwtService.getRefreshToken();
    //   const tokens = await Api.post("token/refresh", {
    //     refresh_token: refreshToken
    //   });

    //   context.dispatch(SAVE_TOKENS, tokens.data);
    //   context.dispatch(START_REFRESH_TIMER);
    // },
    // [START_REFRESH_TIMER](context) {
    //   const issuedAt = JwtService.getTokenIssuedAt();

    //   if (issuedAt) {
    //     const ttl = getExpiryTTL(issuedAt);
    //     const refreshNow = shouldRefresh(issuedAt);

    //     if (ttl < 0 || refreshNow) {
    //       context.dispatch(REFRESH_TOKEN);
    //       return;
    //     }

    //     setTimeout(() => {
    //       context.state.refreshTimer = context.dispatch(REFRESH_TOKEN);
    //     }, ttl * 1000);
    //   }
    // },
    [OFINANS_LOGIN](context, credentials) {
      return new Promise((resolve, reject) => {
        ApiOfinans.post("auth/login", credentials)
          .then(({ data }) => {
            localStorage.setItem("ofinans_token", data.access_token);
            localStorage.setItem("ofinans_data", JSON.stringify(data));
            ApiOfinans.setHeader();
            resolve(data);
          })
          .catch(({ response }) => {
            reject(response);
          });
      });
    },
    [OFINANS_OTP](context, credentials) {
      return new Promise((resolve, reject) => {
        ApiOfinans.post("auth/login/2fa", credentials)
          .then(({ data }) => {
            resolve(data);
          })
          .catch(({ response }) => {
            reject(response);
          });
      });
    },
    [OFINANS_OTP_AGAIN](context, credentials) {
      return new Promise((resolve, reject) => {
        ApiOfinans.post("auth/send-code", credentials)
          .then(({ data }) => {
            resolve(data);
          })
          .catch(({ response }) => {
            reject(response);
          });
      });
    },
    [OTP](context, credentials) {
      return new Promise((resolve, reject) => {
        Api.post("auth/authenticate-otp", credentials)
          .then(({ data }) => {
            context.dispatch(SAVE_TOKENS, data);
            resolve();
          })
          .catch(({ error }) => {
            reject(error);
          });
      });
    },
    [OTP_AGAIN](context, credentials) {
      return new Promise((resolve, reject) => {
        Api.post("auth/resend-otp", credentials)
          .then(({ data }) => {
            resolve(data);
          })
          .catch(({ response }) => {
            reject(response);
          });
      });
    },
    [APP_LOGIN](context, credentials) {
      return new Promise((resolve, reject) => {
        Api.post("auth/login", credentials)
          .then(response => {
            const hasGoogle2FA = response.status === 207;
            resolve(hasGoogle2FA);
          })
          .catch(({ response }) => {
            reject(response);
          });
      });
    },
    [LOGOUT](context) {
      context.commit(PURGE_AUTH);
    },
    [REGISTER](context, credentials) {
      return new Promise(resolve => {
        Api.post("login", credentials)
          .then(({ data }) => {
            context.commit(SET_AUTH, data);
            resolve(data);
          })
          .catch(({ response }) => {
            context.commit(SET_ERROR, response.data.errors);
          });
      });
    },
    [VERIFY_AUTH](context) {
      if (JwtService.getAuthToken()) {
        Api.setHeader();
        Api.get("/me")
          .then(async ({ data }) => {
            // console.log('data:',data);
            // if user is in ROLE_BRANCH set parent's commission and representative
            const parent = deStructureParentData(data.parent);
            if (data?.userRole?.roleRank === "ROLE_BRANCH" && data.merchantId) {
              const getCompany = await context.dispatch(
                "company/GET_COMPANY",
                data.merchantId
              );
              const { commission, representative } = getCompany.data;
              parent.commission = commission > 0 ? commission : 0;
              parent.representative = deStructureParentRepresentativeData(
                representative
              );
              parent.representativeName =
                deStructureParentRepresentativeData(representative)?.name || "";
            }
            context.dispatch("company/SET_MAIN_COMPANY", parent).then(() => {
              context.commit(SET_AUTH, { ...data, parent: parent });
              // context.dispatch(START_REFRESH_TIMER);
            });
          })
          .catch(() => {
            context.dispatch(LOGOUT);
          });
      } else {
        context.commit(PURGE_AUTH);
      }
    },
    [UPDATE_PASSWORD](context, payload) {
      const password = payload;

      return Api.put("password", password).then(({ data }) => {
        context.commit(SET_PASSWORD, data);
        return data;
      });
    }
  },
  getters: {
    activeUser(state) {
      return state.user;
    },
    activeUserId(state) {
      return state.user?.id;
    },
    isAuthenticated(state) {
      return state.isAuthenticated;
    },
    userRole(state) {
      return state.userRole;
    },
    isMerchant(state) {
      return state.userRole?.roleRank === "ROLE_MERCHANT";
    },
    isDistributor(state) {
      return state.userRole?.roleRank === "ROLE_DISTRIBUTOR";
    },
    isBranch(state) {
      return state.userRole?.roleRank === "ROLE_BRANCH";
    }
  }
};

export default auth;
