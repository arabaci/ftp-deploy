/* eslint-disable no-console */
import Api from "../../services/api";
import { ExtractRequestData } from "@/utils/extract-request";
import i18n from "@/plugins/vue-i18n";

const orders = {
  state: () => ({
    orders: [],
    perPage: 1,
    totalItems: 1,
    totalPages: 1,
    selectedOrder: JSON.parse(sessionStorage.getItem("_selectedOrder") || "{}"),
    viewOrder: JSON.parse(sessionStorage.getItem("_viewOrder") || "{}"),
    selectedOrderMerchantId: null,
    orderCompany: {},
    orderCompanyWallet: {},
    nonUserOrderWallet: {}
  }),
  mutations: {
    SET_PER_PAGE(state, perPage) {
      state.perPage = perPage;
    },
    SET_TOTAL_ORDER_ITEMS(state, totalItems) {
      state.totalItems = totalItems;
    },
    SET_TOTAL_PAGES(state, pages) {
      state.totalPages = pages;
    },
    SET_ORDERS(state, payload) {
      state.orders = payload;
    },
    SET_VIEW_ORDER(state, payload) {
      state.viewOrder = payload;
    },
    CLEAR_VIEW_ORDER(state) {
      state.viewOrder = {};
    },
    SET_SELECTED_ORDER(state, payload) {
      state.selectedOrder = payload;
      state.selectedOrderMerchantId = payload.merchantId;
    },
    CLEAR_SELECTED_ORDER(state) {
      state.selectedOrder = {};
      state.selectedOrderMerchantId = null;
    },
    SET_SELECTED_ORDER_COMPANY(state, payload) {
      state.orderCompany = payload;
    },
    SET_SELECTED_ORDER_WALLETS(state, payload) {
      state.orderCompanyWallet = payload;
    },
    SET_NON_USER_ORDER_WALLETS(state, payload) {
      state.nonUserOrderWallet = payload;
      // console.log('SET_NON_USER_ORDER_WALLETS.payload:', payload);
    },
    CLEAR_NON_USER_ORDER_WALLETS(state) {
      state.nonUserOrderWallet = {};
    }
  },
  actions: {
    async GET_ORDERS({ getters, commit }, nextPage = 1) {
      const id = getters.MAIN_COMPANY_ID;

      const orders = await Api.get(
        "companies/" + id + "/company-orders",
        `?page=${nextPage}`,
        Api.JsonApiHeaders
      );

      const { data, totalItems, itemsPerPage, totalPages } = ExtractRequestData(
        orders
      );

      commit("SET_TOTAL_PAGES", totalPages);
      commit("SET_PER_PAGE", itemsPerPage);
      commit("SET_TOTAL_ORDER_ITEMS", totalItems);
      commit("SET_ORDERS", data);
      return data;
    },
    async DELETE_ORDER(state, orderId) {
      await Api.delete("/company-orders/" + orderId);
      state.commit("SET_VIEW_ORDER", {});
    },
    GET_ORDER_DETAILS(state, payload) {
      Api.post("order-detail", payload)
        .then(({ data }) => {
          state.commit("SET_SELECTED_ORDER_COMPANY", data.company);
          state.commit("SET_SELECTED_ORDER_WALLETS", data.wallets);
        })
        .catch(({ response }) => {
          console.error(response);
        });
    },
    UPDATE_SELECTED_QR_CURRENCY(state, payload) {
      Api.post("update-selected-currency", payload)
        .then(() => {
          console.log("UPDATE_SELECTED_QR_CURRENCY:success");
        })
        .catch(({ response }) => {
          console.error(response);
        });
    },
    CREATE_QR_SALE(state, payload) {
      return new Promise((resolve, reject) => {
        Api.post(
          "/company-orders/" + payload.orderId + "/transactions/qr-sale",
          payload,
          {
            headers: { "Content-Type": "application/json", Accept: "*/*" }
          }
        )
          .then(result => {
            resolve(result);
          })
          .catch(({ response }) => {
            reject(response);
          });
      });
    },
    WITHDRAW_QR_SALE(state, payload) {
      Api.get(`/get/nexus-withdraw/${payload.order}/${payload.qrCode}`);
      // .then((data) => {
      //   console.log("data");
      //   console.log(data);
      //   Swal.fire({
      //     icon: "success",
      //     title: self.$t("PAGES.SALES_MANAGEMENT.QR_SALE.ORDER_COMPLETED"),
      //     confirmButtonText: self.$t("FORM.OK"),
      //     allowOutsideClick: false
      //   });
      // })
      // .catch(({ response }) => {
      //   console.log("response");
      //   console.error(response);
      //   Swal.fire({
      //     icon: "warning",
      //     title: self.$t(
      //         "PAGES.SALES_MANAGEMENT.QR_SALE.MISSING_ORDER_PAYMENT"
      //     ),
      //     confirmButtonText: self.$t("FORM.OK"),
      //     allowOutsideClick: false
      //   });
      //
      // });
    },
    async CREATE_ORDER(state, payload) {
      payload.price = payload.price.toString();
      payload.fee = payload.fee.toString();

      return new Promise((resolve, reject) => {
        Api.post("company-orders", payload)
          .then(order => {
            resolve(order.data);
          })
          .catch(({ response }) => {
            console.error(response);
            reject(response);
          });
      });
    },
    GET_ORDER(state, payload) {
      // Api.get("company-orders/"+payload+'/')
      Api.get("company-orders/" + payload)
        .then(order => {
          state.commit("SET_ORDERS", order.data);
          state.commit("SET_SELECTED_ORDER", order.data);
        })
        .catch(({ response }) => {
          console.error(response);
        });
    },
    async GET_ORDERS_TOTAL(state, branchId) {
      const ordersTotals = await Api.get(
        `company-orders/${branchId}/orders-total`
      );
      return ordersTotals.data;
    },
    async SET_SELECTED_ORDER(state, payload) {
      await state.commit("SET_SELECTED_ORDER", payload);
    },
    async SET_VIEW_ORDER(state, payload) {
      await state.commit("SET_VIEW_ORDER", payload);
    },
    async GET_NON_USER_WALLET(state, payload) {
      try {
        const res = await Api.get(
          `/nexus/create_payment/${payload.currency}/${payload.orderId}/${payload.payment}`
        );
        state.commit("SET_NON_USER_ORDER_WALLETS", res.data);
        return res.data;
      } catch (error) {
        throw error;
      }
    },
    CLEAR_NON_USER_WALLET(state) {
      state.commit("CLEAR_NON_USER_ORDER_WALLETS");
    },
    async CHECK_ORDER_STATUS(state, orderId) {
      return await Api.get(`company-orders/${orderId}`);
    },
    async FETCH_BRANCH_SALES_REPORT(state, companyId) {
      const url = `company-orders/${companyId}/branch-sales-report`;
      const fetchedData = await Api.get(url);
      if (fetchedData.data.length > 0) {
        const exportData = fetchedData.data.map(order => ({
          ...order,
          price: `${order.price} ${order?.paymentUnitName || ""}`,
          orderTotal: `${order.orderTotal} ${order?.paymentUnitName || ""}`,
          orderStatus: order?.status
            ? i18n.t(
                `PAGES.SALES_MANAGEMENT.LIST.ORDER_STATUSES.${order.status}`
              )
            : "",
          paymentStateName: order?.paymentStateId
            ? i18n.t(
                `PAGES.SALES_MANAGEMENT.LIST.PAYMENT_STATES[${order.paymentStateId}]`
              )
            : ""
        }));
        return exportData;
      }
      return [];
    },
    CLEAR_VIEW_ORDER(state) {
      state.commit("CLEAR_VIEW_ORDER");
    },
    CLEAR_SELECTED_ORDER({ commit }) {
      commit("CLEAR_SELECTED_ORDER");
    },
    async UPDATE_COMPANY_ORDER(_, { orderId, status }) {
      return await Api.post("update-company-order-status", {
        id: orderId,
        status
      });
    },
    async DELETE_WALLET_ADDRESS(_, address) {
      return await Api.post("delete-wallet-address", { address });
    }
  },
  getters: {
    TOTAL_ORDERS_PAGES(state) {
      return state.totalPages;
    },
    ORDERS_PER_PAGE(state) {
      return state.perPage;
    },
    ORDERS_TOTAL_ITEMS(state) {
      return state.totalItems;
    },
    MAIN_COMPANY_ID(state, getters, rootState, rootGetters) {
      return rootGetters["company/MAIN_COMPANY_ID"];
    },
    VIEW_ORDER(state) {
      return state.viewOrder;
    },
    SELECTED_ORDER(state) {
      return state.selectedOrder;
    },
    SELECTED_ORDER_MERCHANT_ID(state) {
      return state.selectedOrderMerchantId;
    }
  },

  namespaced: true
};

export default orders;
