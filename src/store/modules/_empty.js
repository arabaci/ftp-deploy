const example = {
  state: () => ({}),
  actions: {
    SET_EXAMPLE(state, payload) {
      console.log(state, payload);
    }
  },
  mutations: {
    GET_EXAMPLE({ state, commit }) {
      console.log(state, commit);
    }
  }
};

export default example;
