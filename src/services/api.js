import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "@/services/jwt";

/**
 * Service to call HTTP request via Axios
 */

const Api = {
  init() {
    Vue.use(VueAxios, axios);

    // https://miraclepay.dev.ofinansdev.com/

    // http://mp.api.localhost.com/api/ - local
    // https://mp.api.ofinansdev.com/api/ - live
    // https://payapi.dev.ofinansdev.com/api/ - dev

    Vue.axios.defaults.baseURL =
      process.env.VUE_APP_API_URI || "https://payapi.dev.ofinansdev.com/api/";
  },

  /**
   * Set the default HTTP request headers
   */
  setHeader() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${JwtService.getAuthToken()}`;

    // Vue.axios.defaults.headers.common['Accept'] = 'application/vnd.api+json';
  },

  // headers: {'X-Requested-With': 'XMLHttpRequest'},

  query(resource, params) {
    return Vue.axios.get(resource, params).catch(error => {
      throw new Error(`[KT] ApiService ${error}`);
    });
  },

  /**
   * Send the GET HTTP request
   * @param resource
   * @param slug
   * @returns {*}
   */
  get(resource, slug = "", params) {
    return Vue.axios.get(`${resource}/${slug}`, params).catch(error => {
      throw new Error(`[KT] ApiService ${error}`);
    });
  },

  /**
   * Set the POST HTTP request
   * @param resource
   * @param params
   * @returns {*}
   */
  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },

  /**
   * Send the UPDATE HTTP request
   * @param resource
   * @param slug
   * @param params
   * @returns {IDBRequest<IDBValidKey> | Promise<void>}
   */
  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },

  /**
   * Send the PUT HTTP request
   * @param resource
   * @param params
   * @returns {IDBRequest<IDBValidKey> | Promise<void>}
   */
  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  /**
   * Send the DELETE HTTP request
   * @param resource
   * @returns {*}
   */
  delete(resource) {
    return Vue.axios.delete(resource).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },
  JsonApiHeaders: {
    headers: { Accept: "application/vnd.api+json" }
  }
};

export default Api;
