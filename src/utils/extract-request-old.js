const camelise = value => {
  value = value.toString();
  return value.charAt(0).toLowerCase() + value.slice(1);
};

const extractUri = relationshipData => {
  const relationshipDataType = `${camelise(relationshipData.type)}Uri`;

  return {
    attributeName: relationshipDataType,
    attributeValue: relationshipData.id.substring(5)
  };
};

export const ExtractRequestData = response => {
  const data = response.data.data.map(item => {
    const attributes = item.attributes;
    const relationships = item.relationships;

    if (relationships) {
      Object.values(relationships).forEach((relationship, index) => {
        const relationshipData = relationship.data;

        if (relationshipData.length) {
          const relationshipKey = Object.keys(relationships)[index];
          let relationshipArray = [];

          relationshipData.forEach(arrayData => {
            const { attributeName, attributeValue } = extractUri(arrayData);
            relationshipArray.push({ [attributeName]: attributeValue });
          });
          attributes[relationshipKey] = relationshipArray;
        } else {
          const { attributeName, attributeValue } = extractUri(
            relationshipData
          );
          attributes[attributeName] = attributeValue;
        }
      });
    }

    return attributes;
  });

  const totalItems = response.data.meta.totalItems;
  const itemsPerPage = response.data.meta.itemsPerPage;

  let totalPages = parseInt(totalItems / itemsPerPage);
  const remainder = totalItems % itemsPerPage;

  totalPages = remainder ? totalPages + 1 : totalPages;

  return { data, totalItems, itemsPerPage, totalPages };
};
