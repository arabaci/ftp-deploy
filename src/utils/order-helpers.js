import i18n from "@/plugins/vue-i18n";

export const getOrderBadgeAndLabel = status => {
  let itemClass = null;
  let itemLabel = null;

  const labelRoot = "PAGES.SALES_MANAGEMENT.LIST.ORDER_STATUSES";

  const orderItemClasses = {
    0: "badge-warning",
    1: "badge-success",
    2: "badge-info",
    3: "badge-danger",
    4: "badge-danger",
    5: "badge-danger",
    6: "badge-danger",
    7: "badge-success",
    8: "badge-info",
    9: "badge-info",
    10: "badge-warning",
    11: "badge-danger",
    12: "badge-danger"
  };
  itemClass = orderItemClasses[status];
  itemLabel = i18n.t(`${labelRoot}[${status}]`);

  return { itemClass, itemLabel };
};

export const getTotal = order => {
  const price = parseFloat(order.price);
  const fee = parseFloat(order.fee);
  const isFeeIncludedPrice = order.feeIncluded;

  const feeAmount = (price * fee) / 100;
  const totalAmount = isFeeIncludedPrice
    ? price - feeAmount
    : price + feeAmount;
  return totalAmount.toFixed(2);
};

export const getPaymentUnitName = order => {
  const signs = { usd: "$", gbp: "£", try: "₺", eur: "€" };

  // const isCompleted = +order?.status ;
  const paymentUnitName = order?.paymentUnitName
    ? signs[order.paymentUnitName.toLowerCase()]
    : "";
  return paymentUnitName;
};

export const getOrderPaymentStateBadge = status => {
  let itemClass = null;
  const paymentItemClasses = [
    "badge-warning",
    "badge-info",
    "badge-success",
    "badge-danger",
    "badge-danger",
    "badge-danger",
    "badge-danger"
  ];

  itemClass = paymentItemClasses[status];

  return itemClass;
};

export const getPaidCryptoQuantity = order => {
  return [1, 5].includes(order.status)
    ? order.platform === "Nexus"
      ? order.nexusAmount
      : order.amount
    : "";
};
