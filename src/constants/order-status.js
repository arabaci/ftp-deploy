export const ORDER_STATUS = [
  { id: 0, name: "Pending" },
  { id: 1, name: "Completed" },
  { id: 2, name: "Processing" },
  { id: 3, name: "Failed" },
  { id: 4, name: "Cancelled" },
  { id: 5, name: "Insufficient balance" },
  { id: 6, name: "Refund" }
];
