import { ROLE } from "./roles";

export const ROUTE_TITLE = {
  DASHBOARD: "Dashboard",
  HOME: "Home",
  SALES_MANAGEMENT: "Sales Management",
  ALL_SALES: "All Sales",
  NEW_SALE: "New Sale",
  ORDER_DETAILS: "Order Details",
  WALLET_MANAGEMENT: "Wallet Management",
  WALLETS: "Wallets",
  WITHDRAWALS: "Withdrawals",
  PERSONNEL_MANAGEMENT: "Personnel Management",
  PERSONNEL: "All Personnel",
  NEW_PERSONNEL: "New Personnel",
  COMPANY: "Company",
  SETTINGS: "Settings",
  ROLES: "Roles",
  LOGS: "Logs",
  LOGOUT: "Logout"
};

export const ROUTES = [
  {
    title: "ROUTES.DASHBOARD",
    name: "dashboard",
    route: "/dashboard",
    role: ROLE.ANY
  },
  {
    title: "ROUTES.HOME",
    name: "dashboard",
    route: "/dashboard",
    role: ROLE.ANY
  },
  {
    title: "ROUTES.SALES_MANAGEMENT",
    name: "sales-management",
    route: "/sales-management",
    role: ROLE.BRANCH,
    children: [
      {
        title: "ROUTES.ALL_SALES",
        name: "sales-list",
        route: "list",
        role: ROLE.BRANCH
      },
      {
        title: "ROUTES.ORDER_DETAILS",
        name: "order-details",
        route: "order-details",
        role: ROLE.BRANCH
      },
      {
        title: "ROUTES.NEW_SALE",
        name: "link-sales",
        route: "link-sales",
        role: ROLE.BRANCH
      },
      {
        title: "ROUTES.QR_SALE",
        name: "qr-sale",
        route: "qr-sale",
        role: ROLE.BRANCH
      }
    ]
  },
  {
    title: "ROUTES.WALLET_MANAGEMENT",
    name: "wallet-management",
    route: "/wallet-management",
    role: ROLE.ANY,
    children: [
      { title: "ROUTES.WALLETS", name: "list", route: "list", role: ROLE.ANY },
      {
        title: "ROUTES.WITHDRAWALS",
        name: "withdrawal",
        route: "withdrawal",
        role: ROLE.ANY
      }
    ]
  },
  {
    title: "ROUTES.PERSONNEL_MANAGEMENT",
    name: "personel-management",
    route: "/personel-management",
    role: ROLE.ANY,
    children: [
      {
        title: "ROUTES.PERSONNEL",
        name: "list",
        route: "list",
        role: ROLE.ANY
      },
      {
        title: "ROUTES.NEW_PERSONNEL",
        name: "new",
        route: "new",
        role: ROLE.ANY
      }
    ]
  },
  {
    title: "ROUTES.COMPANY",
    name: "company",
    route: "/company",
    role: ROLE.ANY,
    children: [
      { title: "ROUTES.ROLES", name: "list", route: "", role: ROLE.ANY },
      { title: "ROUTES.LOGS", name: "", route: "", role: ROLE.ANY }
    ]
  },
  {
    title: "ROUTES.SETTINGS",
    name: "settings",
    route: "/settings",
    role: ROLE.ANY,
    children: [
      {
        title: "ROUTES.SETTINGS",
        name: "general",
        route: "general",
        role: ROLE.ANY
      },
      { title: "ROUTES.ROLES", name: "roles", route: "roles", role: ROLE.ANY },
      { title: "ROUTES.LOGS", name: "logs", route: "logs", role: ROLE.ANY }
    ]
  },

  { title: "ROUTES.LOGOUT", name: "", route: "", role: ROLE.ANY }
];
